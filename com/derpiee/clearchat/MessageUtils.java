package com.derpiee.clearchat;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MessageUtils {

	public static void sendMessage(MessageType type, Player p, String message) {
		switch(type) {
		case INFO:
			p.sendMessage(ChatColor.YELLOW + "[ClearChat] " + ChatColor.WHITE + message);
			break;
		case SUCCESS:
			p.sendMessage(ChatColor.GREEN + "[ClearChat] " + ChatColor.WHITE + message);
			break;
		case SEVERE:
			p.sendMessage(ChatColor.DARK_RED + "[ClearChat] " + ChatColor.WHITE + message);
			break;
		case DEBUG:
			p.sendMessage(ChatColor.GRAY + "[ClearChat] " + ChatColor.WHITE + message);
			break;
		}
	}
	
	public static void sendMessage(MessageType type, CommandSender s, String message) {
		switch(type) {
		case INFO:
			s.sendMessage(ChatColor.YELLOW + "[ClearChat] " + ChatColor.WHITE + message);
			break;
		case SUCCESS:
			s.sendMessage(ChatColor.GREEN + "[ClearChat] " + ChatColor.WHITE + message);
			break;
		case SEVERE:
			s.sendMessage(ChatColor.DARK_RED + "[ClearChat] " + ChatColor.WHITE + message);
			break;
		case DEBUG:
			s.sendMessage(ChatColor.GRAY + "[ClearChat] " + ChatColor.WHITE + message);
			break;
		}
	}

	enum MessageType {
		INFO, SUCCESS, SEVERE, DEBUG;
	}
}
