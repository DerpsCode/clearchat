package com.derpiee.clearchat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.derpiee.clearchat.MessageUtils.MessageType;

public class ClearCommands implements CommandExecutor{

	public ClearChat clearChat;

	public ClearCommands(ClearChat clearChat) {
		this.clearChat = clearChat;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(cmd.getName().equalsIgnoreCase("clearchat")) {
			if(args.length == 1) {
				if(args[0].equalsIgnoreCase("?")) {
					MessageUtils.sendMessage(MessageType.INFO, sender, "/cchat version");
					MessageUtils.sendMessage(MessageType.INFO, sender, "/cchat credits");
					MessageUtils.sendMessage(MessageType.INFO, sender, "/cchat cglobal");
					MessageUtils.sendMessage(MessageType.INFO, sender, "/cchat clocal");
					MessageUtils.sendMessage(MessageType.INFO, sender, "/cchat cmute");

					return true;
				}

				if(args[0].equalsIgnoreCase("version")) {
					MessageUtils.sendMessage(MessageType.INFO, sender, "Version: " + clearChat.VERSION);
					return true;
				}

				if(args[0].equalsIgnoreCase("credits")) {
					MessageUtils.sendMessage(MessageType.INFO, sender, "Plugin made by " + ChatColor.GREEN + "Derpiee");
					return true;
				}

				if(args[0].equalsIgnoreCase("cglobal")) {
					if(sender.hasPermission(ClearHelpers.CLEAR_GLOBAL_PERMISSION) || sender.isOp()){
						clearChat.getClearHelpers().clearGlobal(sender);
						return true;
					}
				}

				if(args[0].equalsIgnoreCase("clocal")) {
					if(sender instanceof Player) {
						if(sender.hasPermission(ClearHelpers.CLEAR_LOCAL_PERMISSION) || sender.isOp()){
							Player p = (Player)sender;
							clearChat.getClearHelpers().clearLocal(p);
							return true;
						}
					}
				}

				if(args[0].equalsIgnoreCase("cmute")) {
					if(sender instanceof Player) {
						if(sender.hasPermission(ClearHelpers.MUTE_GLOBAL_PERMISSION) || sender.isOp()){
							ClearHelpers.muteInProgress =! ClearHelpers.muteInProgress;
							if(ClearHelpers.muteInProgress) {
								for(Player p : Bukkit.getOnlinePlayers()) {
									if(!p.isOp() || !p.hasPermission(ClearHelpers.MUTE_OVERRIDE_PERMISSION)) {
										ClearHelpers.mutedPlayers.add(p.getName());
									}
								}
								MessageUtils.sendMessage(MessageType.SUCCESS, sender, "You have muted the chat.");
							} else {
								for(Player p : Bukkit.getOnlinePlayers()) {
									if(ClearHelpers.mutedPlayers.contains(p.getName())) {
										ClearHelpers.mutedPlayers.remove(p.getName());
									}
								}
								MessageUtils.sendMessage(MessageType.SUCCESS, sender, "You have unmuted the chat.");
							}

						}
					}
				}


			}
		}
		return false;
	}



}
