package com.derpiee.clearchat;

import org.bukkit.plugin.java.JavaPlugin;

public class ClearChat extends JavaPlugin{

	//Plugin Version
	public String VERSION = this.getDescription().getVersion();
	public ClearHelpers clearHelpers;
	
	public void onEnable() {
		getCommand("clearchat").setExecutor(new ClearCommands(this));
		getServer().getPluginManager().registerEvents(new ChatListener(this), this);
		getConfig().options().copyDefaults(true);
		saveConfig();
	}

	public void onDisable() {

	}

	public ClearHelpers getClearHelpers() {
		if(clearHelpers == null) {
			clearHelpers = new ClearHelpers(this);
		}
		return clearHelpers;
	}
}
