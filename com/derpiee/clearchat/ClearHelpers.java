package com.derpiee.clearchat;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.derpiee.clearchat.MessageUtils.MessageType;

public class ClearHelpers {

	public ClearChat clearChat;
	
	public ClearHelpers(ClearChat clearChat) {
		this.clearChat = clearChat;
	}
	
	public static String CLEAR_GLOBAL_PERMISSION = "clearchat.cglobal";
	public static String CLEAR_LOCAL_PERMISSION = "clearchat.clocal";
	public static String MUTE_GLOBAL_PERMISSION = "clearchat.mglobal";
	public static String MUTE_OVERRIDE_PERMISSION = "clearchat.moverride";

	public static ArrayList<String> mutedPlayers = new ArrayList<String>();
	public static boolean muteInProgress;

	public void clearGlobal(CommandSender whoCleared) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			for(int i = 0; i < clearChat.getConfig().getInt("lines-to-clear"); i++) {
				p.sendMessage(" ");
			}
			if(clearChat.getConfig().getBoolean("say-who-cleared")) {
				MessageUtils.sendMessage(MessageType.SUCCESS, p, clearChat.getConfig().getString("global-clear-message") + " - " + ChatColor.GREEN + whoCleared.getName());
			} else {
				MessageUtils.sendMessage(MessageType.SUCCESS, p, clearChat.getConfig().getString("global-clear-message"));
			}
		}
	}

	public void clearLocal(Player p) {
		for(int i = 0; i < clearChat.getConfig().getInt("lines-to-clear"); i++) {
			p.sendMessage(" ");
		}
		MessageUtils.sendMessage(MessageType.SUCCESS, p, "Chat has been cleared");
	}
}
