package com.derpiee.clearchat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import com.derpiee.clearchat.MessageUtils.MessageType;

public class ChatListener implements Listener{

	public ClearChat clearChat;
	
	public ChatListener(ClearChat clearChat) {
		this.clearChat = clearChat;
	}
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player p = event.getPlayer();
		if(ClearHelpers.mutedPlayers.contains(p.getName()) && ClearHelpers.muteInProgress) {
			event.setCancelled(true);
			MessageUtils.sendMessage(MessageType.INFO, p, clearChat.getConfig().get("global-mute-message").toString());
		}
	}
	
	@EventHandler
	public void onPlayerCommands(PlayerCommandPreprocessEvent event) {
		Player p = event.getPlayer();
		if(ClearHelpers.mutedPlayers.contains(p.getName()) && ClearHelpers.muteInProgress) {
			for(String s : clearChat.getConfig().getStringList("banned-commands-while-muted")) {
				if(event.getMessage().startsWith("/" + s)) {
					event.setCancelled(true);
					MessageUtils.sendMessage(MessageType.INFO, p, clearChat.getConfig().get("global-mute-message").toString());
				}
			}
		}
	}


	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		if(!ClearHelpers.mutedPlayers.contains(p.getName()) && ClearHelpers.muteInProgress) {
			ClearHelpers.mutedPlayers.add(p.getName());
			MessageUtils.sendMessage(MessageType.INFO, p, clearChat.getConfig().get("global-mute-message").toString());
		}else if(!ClearHelpers.muteInProgress && ClearHelpers.mutedPlayers.contains(p.getName())) {
			ClearHelpers.mutedPlayers.remove(p.getName());
		}
	}
}
